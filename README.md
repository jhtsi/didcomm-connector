# DIDComm Connector WIP

## Description

The DIDCommConnector can be used as a Mediator and Connection Management Service by parties who want to set up trust with another party. The DIDCommConnector uses DIDComm v2 and provides a message layer and a management component for the following two use cases:

- Pairing a cloud solution with a smartphone / app solution
- DIDComm v2 based message protocols

## Badges

TODO: On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

TODO: Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

How to install:

- Execute `go get` in the main directory.
- To install all needed dependencies use the `makefile` and run `make`.

For Development (Linux):

- Install air for `make install-air`
- Add `export $PATH=PATH:your/path/to/go/bin` to path (`~/.bashrc`) or `your/path/to/go/bin` to `/etc/paths`
- Build the libs executing `make build-rust`

## Usage

The application is still under development.

How to run the application:

- To start the application execute `make dev`.

Alternatively the application can be run using following chaing of commands:

```
go build -o dcc ./cmd/api/ 
export LD_LIBRARY_PATH=${PWD}/didcomm/lib
./dcc
```

The application may also be executed inside docker. For that scenarion following steps must be executed:

- build the docker container using `make docker-build`
- run the docker container on the local docker host using `make docker-run`
- start the docker container using `make docker-start` (or use docker desktop)
- optional: before re-building the container, do not forget to clean using `make docker-clean`

## Test

To run all test execute `make test`

### swagger

The swagger is available at the URL: `localhost:9090/swagger/index.html`

## Support

TODO: Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

TODO: If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

TODO: State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

TODO: Show your appreciation to those who have contributed to the project.

## License

TODO: For open source projects, say how it is licensed.

## Project status

The project is in an early phase of development. Therefore it cannot be used as it is now.
The API is not final.

### Debugging
While under WSL to be able to use the websocket, please do not use localhost from the Windows machine. Instead use the given ip address given by `ip add | grep "eth0"`