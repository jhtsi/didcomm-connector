-- TODO: dont use this migration in production
-- This migration drops all tables and recreates them for testing purposes
-- change current workflow of creating tables in docker compose

-- Creating Keyspace
CREATE KEYSPACE IF NOT EXISTS dcc WITH REPLICATION = {
  'class' : 'SimpleStrategy', 
  'replication_factor' : 1
};

-- Dropping Tables
DROP TABLE IF EXISTS mediatees;
DROP TABLE IF EXISTS mediator_did;
DROP TABLE IF EXISTS recipient_dids;
DROP TABLE IF EXISTS deny_list;
DROP TABLE IF EXISTS secret_types;
DROP TABLE IF EXISTS secrets;
DROP TABLE IF EXISTS messages;

-- Creating Tables
CREATE TABLE IF NOT EXISTS mediatees (
  remote_did text,
  routing_key text,
  created TIMESTAMP,
  protocol text,
  topic text,
  properties map<text, text>,
  PRIMARY KEY (remote_did)
);

CREATE TABLE IF NOT EXISTS mediator_did (
  did text,
  created TIMESTAMP,
  PRIMARY KEY (did)
);

CREATE TABLE IF NOT EXISTS recipient_dids (
  recipient_did text,
  remote_did text,
  PRIMARY KEY (recipient_did)
);

CREATE INDEX IF NOT EXISTS recipient_dids_remote_did_idx ON recipient_dids (remote_did);

CREATE TABLE IF NOT EXISTS deny_list (
  did text,
  created TIMESTAMP,
  PRIMARY KEY (did)
);

CREATE TABLE IF NOT EXISTS secret_types (
  id int,
  description text,
  PRIMARY KEY (id)
);

-- Inserting into Secret Types Table
INSERT INTO secret_types(id, description) VALUES (1,'SecretTypeJsonWebKey2020');
INSERT INTO secret_types(id, description) VALUES (2,'SecretTypeX25519KeyAgreementKey2019');
INSERT INTO secret_types(id, description) VALUES (3,'SecretTypeEd25519VerificationKey2018');
INSERT INTO secret_types(id, description) VALUES (4,'SecretTypeEcdsaSecp256k1VerificationKey2019');
INSERT INTO secret_types(id, description) VALUES (5,'SecretTypeX25519KeyAgreementKey2020');
INSERT INTO secret_types(id, description) VALUES (6,'SecretTypeEd25519VerificationKey2020');
INSERT INTO secret_types(id, description) VALUES (7,'SecretTypeOther');

CREATE TABLE IF NOT EXISTS secrets (
  id text,
  type int,
  key text,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS messages (
  message_id text,
  recipient_did text,
  description text,
  filename text,
  media_type text,
  format text,
  lastmod_time bigint,
  byte_count bigint,
  attachment_data text,
  PRIMARY KEY (message_id)
);

CREATE INDEX IF NOT EXISTS messages_recipient_did_idx ON messages (recipient_did);
