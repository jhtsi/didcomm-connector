package main

import (
	"net/http"
	"net/url"

	"gaiax/didcommconnector/didcomm"
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/protocol"
	"gaiax/didcommconnector/service"

	"github.com/gin-gonic/gin"
	"github.com/gocql/gocql"
)

// TODO: update swagger documentation
// PingExample 	godoc
// @Summary 	Create a new connection
// @Schemes
// @Description Creates connection endpoint (more to be added)
// @Tags 		Connections
// @Accept 		json
// @Produce 	json
// @Param 		did	path	string	true "DID"
// @Success 	201 {object} model.Connection
// @Router 		/connections/{did} [post]
func (app *application) CreateConnection(context *gin.Context) {
	context.Header("Content-Type", "application/json")
	type body struct {
		Protocol   string            `json:"protocol"`
		RemoteDid  string            `json:"did"`
		Topic      string            `json:"topic"`
		Properties map[string]string `json:"properties"`
	}
	var b body
	err := context.BindJSON(&b)
	if err != nil {
		_ = app.SendPr(context, protocol.PR_INVALID_REQUEST, err)
		return
	}
	err = app.mediator.ConnectionManager.CreateConnectionInvitation(b.Protocol, b.RemoteDid, b.Topic, b.Properties)
	if err != nil {
		_ = app.SendPr(context, protocol.PR_ALREADY_CONNECTED, err)
		return
	}
	oob := protocol.NewOutOfBand(app.mediator)
	invitation, err := oob.Handle()
	if err != nil {
		app.logger.Error("CreateConnectionEndpoint", err)
		context.Status(http.StatusBadRequest)
	}
	context.String(http.StatusOK, invitation)
}

// PingExample 	godoc
// @Summary 	Update existing connection
// @Schemes
// @Description Updates existing connection (more to be added)
// @Tags 		Connections
// @Accept 		json
// @Produce 	json
// @Param 		did	body	model.Connection	true "DID"
// @Success 	204 {string}	string	"Updated"
// @Router 		/connections  [patch]
func (app *application) UpdateConnection(context *gin.Context) {
	did := context.Param("did")
	app.logger.Info("UpdateConnection", "did", did)
	context.Status(http.StatusNotImplemented)
}

// PingExample	godoc
// @Summary 	Delete connection
// @Schemes
// @Description	Deletes connection (more to be added)
// @Tags 		Connections
// @Accept 		json
// @Produce 	json
// @Param 		did	path	string	true "DID"
// @Success 	204 {string} string "Deleted"
// @Router 		/connections/{did} [delete]
func (app *application) DeleteConnection(context *gin.Context) {
	did := context.Param("did")
	cs := service.CassandraService{}
	d, err := gocql.ParseUUID(did)

	cs.Delete(d)

	if err != nil {
		app.logger.Error("DeleteConnection", err)
		context.Status(http.StatusBadRequest)
	}
	context.Status(http.StatusOK)
}

// PingExample	godoc
// @Summary		Get connection information
// @Schemes
// @Description	Returns existing connection information (more to be added)
// @Tags 		Connections
// @Accept 		json
// @Produce 	json
// @Param 		did	path	string	true "DID"
// @Success 	200 {object} model.Connection
// @Router 		/connections/{did} [get]
func (app *application) ConnectionInformation(context *gin.Context) {
	did := context.Param("did")
	did, err := url.QueryUnescape(did)
	if err != nil {
		context.Status(http.StatusBadRequest)
		return
	}
	app.logger.Info("ConnectionInformation", "did", did)
	context.Status(http.StatusNotImplemented)
}

// PingExample	godoc
// @Summary		Get connections
// @Schemes
// @Description	Returns a list with the existing connections (more to be added)
// @Tags 		Connections
// @Accept		json
// @Produce		json
// @Success		200 {array} model.Connection
// @Router		/connections [get]
func (app *application) ListConnections(context *gin.Context) {
	cs := service.CassandraService{}
	all, err := cs.GetAll()
	if err != nil {
		context.Status(http.StatusBadRequest)
	}
	context.IndentedJSON(http.StatusOK, all)
}

// PingExample	godoc
// @Summary		Get connection status
// @Schemes
// @Description	Returns if a connections is blocked or not
// @Tags 		Connections
// @Accept 		json
// @Produce 	json
// @Param 		did	path	string	true "DID"
// @Success 	200 {object} model.ConnectionStatus
// @Router 		/connections/status/{did} [get]
func (app *application) ConnectionStatus(context *gin.Context) {
	did := context.Param("did")
	app.logger.Info("ConnectionStatus", "did", did)
	context.Status(http.StatusNotImplemented)
}

// PingExample	godoc
// @Summary 	Block existing connection
// @Schemes
// @Description	Blocks existing connection (more to be added)
// @Tags 		Connections
// @Accept 		json
// @Produce		json
// @Param 		did	path	string	true "DID"
// @Success 	204 {string} string "Blocked"
// @Router 		/connections/block/{did} [post]
func (app *application) BlockConnection(context *gin.Context) {
	did := context.Param("did")
	app.logger.Info("BlockConnection", "did", did)
	context.Status(http.StatusNotImplemented)
}

func (app *application) SendPr(context *gin.Context, pr didcomm.Message, err error) error {
	if err != nil {
		config.Logger.Error("Problem Report", "err", err)
	}
	packMsg, err := app.mediator.PackPlainMessage(pr)
	if err != nil {
		config.Logger.Error("Problem Report", "err", err)
		context.Status(http.StatusBadRequest)
		return err
	}
	context.String(http.StatusBadRequest, packMsg)
	return nil
}
