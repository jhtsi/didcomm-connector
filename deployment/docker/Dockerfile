FROM rust:1.70 as rust-build

COPY --from=files didcomm-rust /src
WORKDIR /src

#RUN build the rust library
RUN cd uniffi/ && cargo build --release

FROM golang:1.21 as go-build

# Copies your source code into the go directory
COPY --from=files . /src

# set work directory
WORKDIR /src

# copy lib from rust build
RUN mkdir -p didcomm/lib
COPY --from=rust-build /src/uniffi/target/release/libdidcomm_uniffi.* didcomm/lib

#RUN go build to build the application with the name app
RUN go build -o /src/app ./cmd/api/

FROM scratch
# copy lib and dependencies (finde dependencies with: ldd <executable>)
COPY --from=go-build /src/didcomm/lib /bin/lib
COPY --from=go-build /lib/x86_64-linux-gnu/libgcc_s.so.1 /lib/x86_64-linux-gnu/libgcc_s.so.1
COPY --from=go-build /lib/x86_64-linux-gnu/libpthread.so.0 /lib/x86_64-linux-gnu/libpthread.so.0
COPY --from=go-build /lib/x86_64-linux-gnu/libm.so.6 /lib/x86_64-linux-gnu/libm.so.6
COPY --from=go-build /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libdl.so.2
COPY --from=go-build /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu/libc.so.6
COPY --from=go-build /lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2

# copy config file
COPY --from=go-build /src/config.yaml /config.yaml
# copy binary
COPY --from=go-build /src/app /bin/app
# set enviroment variable for lib
ENV LD_LIBRARY_PATH=/bin/lib

EXPOSE 9090:9090

CMD ["/bin/app"]