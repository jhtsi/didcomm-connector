package config

import (
	"errors"
	"fmt"
	"log/slog"
	"os"
	"strings"
	"time"

	"github.com/spf13/viper"
)

const (
	ENV_DEV  = "DEV"
	ENV_PROD = "PROD"

	HTTP   = "http"
	NATS   = "nats"
	HYBRID = "hybrid"
)

type TemplateConfiguration struct {
	Env      string `mapstructure:"env"`
	LogLevel string `mapstructure:"logLevel"`
	Port     int    `mapstructure:"port"`
	Url      string `mapstructure:"url"`

	DidComm struct {
		ResolverUrl        string `mapstructure:"resolverUrl"`
		IsMessageEncrypted bool   `mapstructure:"messageEncrypted"`
	} `mapstructure:"didcomm"`

	CloudForwarding struct {
		Type         string        `mapstructure:"type"`
		EndpointHttp string        `mapstructure:"endpointHttp"`
		EndpointNats string        `mapstructure:"endpointNats"`
		Subject      string        `mapstructure:"subject"`
		TimeoutInSec time.Duration `mapstructure:"timeoutInSec"`
	} `mapstructure:"cloudForwarding"`

	Database struct {
		InMemory bool   `mapstructure:"inMemory"`
		Host     string `mapstructure:"host"`
		Port     int    `mapstructure:"port"`
		User     string `mapstructure:"user"`
		Password string `mapstructure:"password"`
		Keyspace string `mapstructure:"keyspace"`
		DBName   string `mapstructure:"dbName"`
	} `mapstructure:"db"`

	CloudEvents struct {
		Topic string `mapstructure:"topic"`
	} `mapstructure:"cloudevents"`
}

var CurrentConfiguration TemplateConfiguration
var Logger *slog.Logger
var env string

func LoadConfig() error {
	setDefaults()
	readConfig()
	if err := viper.Unmarshal(&CurrentConfiguration); err != nil {
		return err
	}

	setEnvironment()
	if err := checkMode(); err != nil {
		return err
	}
	setLogLevel()
	return nil
}

func IsDev() bool {
	return env == ENV_DEV
}

func IsProd() bool {
	return env == ENV_PROD
}

func IsForwardTypeNats() bool {
	return CurrentConfiguration.CloudForwarding.Type == NATS
}

func IsForwardTypeHybrid() bool {
	return CurrentConfiguration.CloudForwarding.Type == HYBRID
}

func readConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	viper.SetEnvPrefix("BOILERPLATE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if errors.As(err, &configFileNotFoundError) {
			slog.Warn("Configuration not found but environment variables will be taken into account.")
		}
	}
	viper.AutomaticEnv()
}

func setDefaults() {
	viper.SetDefault("env", ENV_DEV)
	viper.SetDefault("logLevel", "info")
	viper.SetDefault("port", 9090)
	viper.SetDefault("url", "http://localhost:9090")
	viper.SetDefault("cloudForwarding.type", "http")
	viper.SetDefault("didcomm.messageEncrypted", false)
}

func setEnvironment() {
	switch strings.ToUpper(CurrentConfiguration.Env) {
	case ENV_DEV:
		env = ENV_DEV
	case ENV_PROD:
		env = ENV_PROD
	default:
		env = ENV_DEV
	}
}

func setLogLevel() {
	logLevel := new(slog.LevelVar)
	switch strings.ToLower(CurrentConfiguration.LogLevel) {
	case "debug":
		logLevel.Set(slog.LevelDebug)
	case "info":
		logLevel.Set(slog.LevelInfo)
	case "warn":
		logLevel.Set(slog.LevelWarn)
	case "error":
		logLevel.Set(slog.LevelError)
	default:
		logLevel.Set(slog.LevelWarn)
	}

	Logger = slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: logLevel,
	}))
	Logger.Info(fmt.Sprintf("loglevel set to %s", logLevel.Level().String()))
}

func checkMode() error {
	selectedTyp := CurrentConfiguration.CloudForwarding.Type
	switch strings.ToLower(selectedTyp) {
	case HTTP:
	case NATS:
	case HYBRID:
	default:
		return fmt.Errorf("Unknow cloud forwarding type %s. Select Type: %s, %s or %s", selectedTyp, HTTP, NATS, HYBRID)
	}

	return nil
}
