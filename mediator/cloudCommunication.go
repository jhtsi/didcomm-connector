package mediator

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gaiax/didcommconnector/didcomm"
	"gaiax/didcommconnector/internal/config"
	"net/http"
	"sync"

	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/nats-io/nats.go"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
)

func SendMessage(message didcomm.Attachment) error {

	messageB64 := message.Data.(didcomm.AttachmentDataBase64).Value.Base64
	messageDecoded, err := base64.StdEncoding.DecodeString(messageB64)
	if err != nil {
		config.Logger.Error("Error decoding message", "msg", err)
	}
	messageStr := string(messageDecoded)

	switch config.CurrentConfiguration.CloudForwarding.Type {
	case config.HTTP:
		return sendCloudEvent(messageStr)
	case config.NATS:
		return sendCloudEvent(messageStr)
	case config.HYBRID:
		return sendHybridMessage(messageStr)
	default:
		return sendCloudEvent(messageStr)
	}
}

func ReceiveMessage() {

	topic := config.CurrentConfiguration.CloudForwarding.Subject

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, topic)
	if err != nil {
		config.Logger.Error("Unable to connect to cloudprovider", "msg", err)
	}

	defer client.Close()

	// Use a WaitGroup to wait for a message to arrive
	wg := sync.WaitGroup{}
	wg.Add(1)

	config.Logger.Info("Receiving cloud events")

	err = client.Sub(func(event event.Event) {
		config.Logger.Info(fmt.Sprintf("%v", event))
		if event.DataBase64 {
			// TODO
			// decode base64 to string
			// handle data (didcomm message)
			config.Logger.Warn("Received a cloud event but handling cloud events is not implemented yet")
		} else {
			// TODO
			// handle data (didcomm message)
			config.Logger.Warn("Received a cloud event but handling cloud events is not implemented yet")
		}
	})
	if err != nil {
		config.Logger.Error("Error in subscribtion of cloud event", "msg", err)
	}

	// Wait for a message to come in
	wg.Wait()
}

func sendHybridMessage(message string) error {
	// TODO: check mediatee forwarding type
	mediateeProtocolType := "http"

	switch mediateeProtocolType {
	case config.HTTP:
		return sendHttpMessage(message)
	case config.NATS:
		return sendNatsMessage(message)
	default:
		// TODO: return error
	}

	return nil
}

func sendCloudEvent(message string) (err error) {

	// TODO: get topic from db mediatee
	topic := "subject.test"

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Pub, topic)
	if err != nil {
		config.Logger.Error("Can not create cloudevent client", "msg", err)
		return
	}
	defer client.Close()

	url := config.CurrentConfiguration.CloudForwarding.EndpointNats

	data := map[string]any{"message": message}
	dataJson, err := json.Marshal(data)
	if err != nil {
		config.Logger.Error("Failed to marshal data", "msg", err)
		return
	}

	event, err := cloudeventprovider.NewEvent(url, "subject.test", dataJson)
	if err != nil {
		config.Logger.Error("Failed to create cloud event", "msg", err)
		return
	}

	if err = client.Pub(event); err != nil {
		config.Logger.Error("Failed to send cloud event", "msg", err)
		return
	}

	return
}

func sendHttpMessage(message string) error {

	endpoint := config.CurrentConfiguration.CloudForwarding.EndpointHttp

	body, err := base64.StdEncoding.DecodeString(message)
	if err != nil {
		return err
	}
	readerBytes := bytes.NewReader(body)
	resp, err := http.Post(endpoint, "application/didcomm-plain+json", readerBytes)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return errors.New("Forwarded message was not accepted by the next recipient")
	}

	return nil
}

func sendNatsMessage(message string) error {

	// TODO: get topic from db mediatee
	endpoint := config.CurrentConfiguration.CloudForwarding.EndpointNats
	topic := config.CurrentConfiguration.CloudForwarding.Subject

	nc, err := nats.Connect(endpoint)
	if err != nil {
		errMsg := fmt.Sprintf("Unable to connect to NATS endpoint %s", endpoint)
		config.Logger.Error(errMsg, err)
		return err
	}
	defer nc.Close()

	if err := nc.Publish(topic, []byte(message)); err != nil {
		errMsg := fmt.Sprintf("Unable to publish to NATS endpoint %s with subject %s", endpoint, topic)
		config.Logger.Error(errMsg, err)
		return err
	}

	return nil
}
