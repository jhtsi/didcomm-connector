package connectionmanager

import (
	"errors"
	"fmt"
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/mediator/database"
)

type ConnectionManager struct {
	database database.Adapter
}

func NewConnectionManager(database database.Adapter) *ConnectionManager {
	return &ConnectionManager{
		database: database,
	}
}

func (c *ConnectionManager) CreateConnectionInvitation(protocol string, remoteDid string, topic string, properties map[string]string) (err error) {
	switch config.CurrentConfiguration.CloudForwarding.Type {
	case config.HTTP:
		if protocol != config.HTTP {
			return errors.New(fmt.Sprintf("protocol %s not supported", protocol))
		}
	case config.NATS:
		if protocol != config.NATS {
			return errors.New(fmt.Sprintf("protocol %s not supported", protocol))
		}
	case "hybrid":
		if protocol != config.HTTP && protocol != config.NATS {
			return errors.New(fmt.Sprintf("protocol %s not supported", protocol))
		}
	default:
		return errors.New(fmt.Sprintf("protocol %s not supported", protocol))
	}
	isMediated, err := c.database.IsMediated(remoteDid)
	if err != nil {
		return err
	}
	if isMediated {
		return errors.New("connection already exists")
	}
	err = c.database.AddMediatee(remoteDid, protocol, topic, properties)
	if err != nil {
		return err
	}
	return nil
}
