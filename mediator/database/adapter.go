package database

import "gaiax/didcommconnector/didcomm"

type Adapter interface {
	GetMediatorDid() (mediatorDid string, err error)
	StoreMediatorDid(mediatorDid string) (err error)
	AddMediatee(remoteDid string, protocol string, topic string, properties map[string]string) (err error)
	SetRoutingKey(remoteDid string, routingKey string) (err error)
	GetRoutingKey(remoteDid string) (routingKey string, err error)
	IsMediated(remoteDid string) (isMediated bool, err error)
	IsRecipientDidRegistered(recipientDid string) (isRecDidRegistered bool, err error)
	GetRecipientDids(remoteDid string) (recipientDids []string, err error)
	RecipientAndRemoteDidBelongTogether(recipientDid string, remoteDid string) (belongTogether bool, err error)
	AddRecipientDid(remoteDid string, recipientDid string) (err error)
	DeleteRecipientDid(remoteDid string, recipientDid string) (err error)
	RemoteDidBelongsToMessage(remoteDid string, messageId string) (didBelongsToMessage bool, err error)
	GetMessageCountForRecipient(recipientDid string) (count int, err error)
	GetMessagesForRecipient(recipientDid string, limit int) (attachements []didcomm.Attachment, err error)
	DeleteMessagesByIds(messageIds []string) (deletedCount int, err error)
	AddMessage(recipientDid string, message didcomm.Attachment) (err error)
}
