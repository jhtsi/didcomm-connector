// Package docs Code generated by swaggo/swag. DO NOT EDIT
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "termsOfService": "http://swagger.io/terms/",
        "contact": {
            "name": "API Support",
            "url": "http://www.change.this/url",
            "email": "email@todo.io"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/connections": {
            "get": {
                "description": "Returns a list with the existing connections (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Get connections",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/model.Connection"
                            }
                        }
                    }
                }
            },
            "patch": {
                "description": "Updates existing connection (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Update existing connection",
                "parameters": [
                    {
                        "description": "DID",
                        "name": "did",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/model.Connection"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "Updated",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/connections/block/{did}": {
            "post": {
                "description": "Blocks existing connection (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Block existing connection",
                "parameters": [
                    {
                        "type": "string",
                        "description": "DID",
                        "name": "did",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "204": {
                        "description": "Blocked",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/connections/status/{did}": {
            "get": {
                "description": "Returns connection status (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Get connection status",
                "parameters": [
                    {
                        "type": "string",
                        "description": "DID",
                        "name": "did",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/model.ConnectionStatus"
                        }
                    }
                }
            }
        },
        "/connections/{did}": {
            "get": {
                "description": "Returns existing connection information (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Get connection information",
                "parameters": [
                    {
                        "type": "string",
                        "description": "DID",
                        "name": "did",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/model.Connection"
                        }
                    }
                }
            },
            "post": {
                "description": "Creates connection endpoint (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Create a new connection",
                "parameters": [
                    {
                        "type": "string",
                        "description": "DID",
                        "name": "did",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Created",
                        "schema": {
                            "$ref": "#/definitions/model.Connection"
                        }
                    }
                }
            },
            "delete": {
                "description": "Deletes connection (more to be added)",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Connections"
                ],
                "summary": "Delete connection",
                "parameters": [
                    {
                        "type": "string",
                        "description": "DID",
                        "name": "did",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "204": {
                        "description": "Deleted",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/message": {
            "patch": {
                "description": "TODO",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Message"
                ],
                "summary": "TODO",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "model.Connection": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "testProperty": {
                    "type": "string"
                }
            }
        },
        "model.ConnectionStatus": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "testProperty": {
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "description": "Description for what is this security definition being used",
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        },
        "BasicAuth": {
            "type": "basic"
        },
        "OAuth2AccessCode": {
            "type": "oauth2",
            "flow": "accessCode",
            "authorizationUrl": "https://example.com/oauth/authorize",
            "tokenUrl": "https://example.com/oauth/token",
            "scopes": {
                "admin": "\t\t\t\t\t\t\tGrants read and write access to administrative information"
            }
        },
        "OAuth2Application": {
            "type": "oauth2",
            "flow": "application",
            "tokenUrl": "https://example.com/oauth/token",
            "scopes": {
                "admin": "\t\t\t\t\t\t\tGrants read and write access to administrative information",
                "write": "\t\t\t\t\t\t\tGrants write access"
            }
        },
        "OAuth2Implicit": {
            "type": "oauth2",
            "flow": "implicit",
            "authorizationUrl": "https://example.com/oauth/authorize",
            "scopes": {
                "admin": "\t\t\t\t\t\t\tGrants read and write access to administrative information",
                "write": "\t\t\t\t\t\t\tGrants write access"
            }
        },
        "OAuth2Password": {
            "type": "oauth2",
            "flow": "password",
            "tokenUrl": "https://example.com/oauth/token",
            "scopes": {
                "admin": "\t\t\t\t\t\t\tGrants read and write access to administrative information",
                "read": "\t\t\t\t\t\t\t\tGrants read access",
                "write": "\t\t\t\t\t\t\tGrants write access"
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "localhost:9090",
	BasePath:         "/",
	Schemes:          []string{},
	Title:            "DIDComm Connector API",
	Description:      "This is a sample server celler server.",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
